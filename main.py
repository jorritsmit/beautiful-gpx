# TODO remove magic numbers
#   elevation box (related to aspect i think)
#   background box
# TODO make GUI:
# https://plotly.com/javascript/react/
# https://pyinstaller.org/en/stable/
#   zoom in and out map
#   add annotation by clicking on map
#   text boxes for titles etc
#   color selection
#   export button
#   Add choice of basemaps, inlcuding layering and opacity
import gpxpy
import gpxpy.gpx
import pandas as pd
import plotly.graph_objects as go
import plotly.io as pio
import requests
import xyzservices.providers as xyz
from io import BytesIO
from pytz import timezone  # type: ignore
from timezonefinder import TimezoneFinder
from PIL import Image
import numpy as np
import math
import logging
import base64
import os
from typing import TypedDict
import json


# Define interfaces for functions
class LatLonBounds(TypedDict):
    min_lon: float
    max_lon: float
    min_lat: float
    max_lat: float


class TileBounds(TypedDict):
    left_xtile: int
    right_xtile: int
    top_ytile: int
    bot_ytile: int


class LatLonPoint(TypedDict):
    lon: float
    lat: float


class TileId(TypedDict):
    xtile: int
    ytile: int


class GPXBoundsBound:
    def __init__(
        self,
        min_latitude: float,
        max_latitude: float,
        min_longitude: float,
        max_longitude: float,
    ) -> None:
        self.min_latitude = min_latitude
        self.max_latitude = max_latitude
        self.min_longitude = min_longitude
        self.max_longitude = max_longitude


class GpxData(TypedDict):
    date: list[str]
    distance: str
    moving_time: str
    start_time_local: str
    end_time_local: str
    average_moving_speed: str
    average_heartrate: str
    elevation_gain: str
    elevation_loss: str


# Configure logging
logging.basicConfig(level=logging.INFO)


class GPXPlotter:

    def __init__(self, gpx_file: str, annotations_file_path: str, config_file_path: str | None = None):
        self.gpx_file = gpx_file

        default_config = {
            "gpx_margin": 0.2,
            "aspect_ratio": 1 / 1.414,
            "primary_text_color": "rgb(26, 36, 48)",
            "primary_accent_color": "rgb(82, 131, 178)",
            "secondary_accent_color": "rgb(82, 131, 178)",
            "elevation_color": "rgb(5, 70, 14)",
            "elevation_color_shadow": "hsl(128, 58%, 24%)",
            "elevation_x_domain": [0.075, 0.425],
            "elevation_y_domain": [0.09, 0.25],
            "titles_y_pos": 0.9,
        }
        if config_file_path is None:
            self.config: dict = default_config
        else:
            with open(config_file_path, "r") as f:
                self.config = json.load(f)

        with open(annotations_file_path, "r") as f:
            self.annotations: dict = json.load(f)

    def lat_lon_to_tile(self, latlon: LatLonPoint, zoom: int) -> TileId:
        lat_rad = np.radians(latlon["lat"])
        n = 1 << zoom
        xtile = int((latlon["lon"] + 180.0) / 360.0 * n)
        ytile = int((1.0 - np.asinh(np.tan(lat_rad)) / np.pi) / 2.0 * n)
        return {"xtile": xtile, "ytile": ytile}

    def tile_to_lat_lon(self, tile_id: TileId, zoom: int) -> LatLonPoint:
        n = 2.0**zoom
        lon = tile_id["xtile"] / n * 360.0 - 180.0
        lat_rad = np.arctan(np.sinh(np.pi * (1 - 2 * tile_id["ytile"] / n)))
        lat = np.degrees(lat_rad)
        return {"lon": lon, "lat": lat}

    def extract_gpx_data(self, gpx: gpxpy.gpx.GPX) -> tuple[pd.DataFrame, GpxData]:
        points = []
        points_data = gpx.get_points_data(distance_2d=False)
        point_no = 0
        for track in gpx.tracks:
            for segment in track.segments:
                for point in segment.points:
                    # get HR data
                    hr = 0
                    for extension in list(point.extensions[0]):
                        if "hr" in extension.tag:
                            hr = int(extension.text)
                            break
                    points.append(
                        (
                            point.latitude,
                            point.longitude,
                            point.elevation,
                            points_data[point_no].distance_from_start,
                            point.time,
                            hr,
                        )
                    )
                    point_no = point_no + 1
        df = pd.DataFrame(
            points, columns=["latitude", "longitude", "elevation", "distance_from_start", "time", "heartrate"]
        )
        tf = TimezoneFinder()
        tz_str = tf.timezone_at(lng=df["longitude"].iat[0], lat=df["latitude"].iat[0])
        if tz_str is None:
            raise Exception("Timezone could not be found")
        tz = timezone(tz_str)
        df["time"] = pd.to_datetime(df["time"])
        df["time"].apply(lambda dt: dt.replace(tzinfo=tz))

        data = GpxData(
            date=[
                df["time"].iloc[0].astimezone(tz).strftime("%#d %B %Y"),
                df["time"].iloc[-1].astimezone(tz).strftime("%#d %B %Y"),
            ],
            distance=f"{gpx.get_moving_data().moving_distance / 1000:.1f} km",
            moving_time=f"{int(gpx.get_moving_data().moving_time // 3600):02d}:{int((gpx.get_moving_data().moving_time % 3600) // 60):02d}",
            start_time_local=df["time"].iloc[0].astimezone(tz).strftime("%H:%M"),
            end_time_local=df["time"].iloc[-1].astimezone(tz).strftime("%H:%M"),
            average_moving_speed=f"{(gpx.get_moving_data().moving_distance / gpx.get_moving_data().moving_time) * 3.6:.1f} km/h",
            average_heartrate=f"{df['heartrate'].mean(skipna=True):.0f} bpm",
            elevation_gain=f"{gpx.get_uphill_downhill().uphill:.0f} m",
            elevation_loss=f"{gpx.get_uphill_downhill().downhill:.0f} m",
        )

        return df, data

    def calculate_map_margins(self) -> tuple[float, float]:
        upper_margin = 1 - self.config["titles_y_pos"] - 0.2  # FIXME: magic number
        lower_margin = max(self.config["elevation_y_domain"])

        return (upper_margin, lower_margin)

    def download_map_tiles(
        self, gpx_bounds: GPXBoundsBound, zoom: int = 14, cache_dir: str = "tile_cache", scale: str = "@2x"
    ) -> tuple[list[list[Image.Image]], TileBounds]:
        api_key = "fd60f6878fb845b290a8bc04013d99d8"
        provider = xyz.Thunderforest.MobileAtlas  # type:ignore

        top_left_tile_id = self.lat_lon_to_tile(
            LatLonPoint(lat=gpx_bounds.max_latitude, lon=gpx_bounds.min_longitude), zoom
        )
        bot_right_tile_id = self.lat_lon_to_tile(
            LatLonPoint(lat=gpx_bounds.min_latitude, lon=gpx_bounds.max_longitude), zoom
        )

        tile_xspan = bot_right_tile_id["xtile"] - top_left_tile_id["xtile"]
        tile_yspan = bot_right_tile_id["ytile"] - top_left_tile_id["ytile"]

        upper_margin, lower_margin = self.calculate_map_margins()

        if tile_xspan > tile_yspan * (self.config["aspect_ratio"] / (1 - upper_margin - lower_margin)):
            tile_yspan_aspect: int = math.ceil(tile_xspan / self.config["aspect_ratio"])
            tile_yspan_diff: int = math.ceil((tile_yspan_aspect - tile_yspan) / 2)
            top_left_tile_id["ytile"] = max(top_left_tile_id["ytile"] - tile_yspan_diff, 0)
            bot_right_tile_id["ytile"] = bot_right_tile_id["ytile"] + tile_yspan_diff
        else:
            tile_yspan_diff = math.ceil((tile_yspan / (1 - upper_margin - lower_margin)) / 2)
            top_left_tile_id["ytile"] = max(top_left_tile_id["ytile"] - tile_yspan_diff, 0)
            bot_right_tile_id["ytile"] = bot_right_tile_id["ytile"] + tile_yspan_diff

            tile_yspan = bot_right_tile_id["ytile"] - top_left_tile_id["ytile"]

            tile_xspan_aspect = tile_yspan * self.config["aspect_ratio"]
            tile_xspan_diff: int = math.ceil((tile_xspan_aspect - tile_xspan) / 2)
            top_left_tile_id["xtile"] = max(top_left_tile_id["xtile"] - tile_xspan_diff, 0)
            bot_right_tile_id["xtile"] = bot_right_tile_id["xtile"] + tile_xspan_diff

        tile_bounds_margin: TileBounds = {
            "left_xtile": max(top_left_tile_id["xtile"] - 1, 0),
            "right_xtile": bot_right_tile_id["xtile"] + 1,
            "top_ytile": max(top_left_tile_id["ytile"] - 1, 0),
            "bot_ytile": bot_right_tile_id["ytile"] + 1,
        }

        if not os.path.exists(cache_dir):
            os.makedirs(cache_dir)

        tiles: list[list[Image.Image]] = []
        for y in range(tile_bounds_margin["top_ytile"], tile_bounds_margin["bot_ytile"] + 1):
            row: list[Image.Image] = []
            for x in range(tile_bounds_margin["left_xtile"], tile_bounds_margin["right_xtile"] + 1):
                tile_path = os.path.join(cache_dir, f"{provider['name']}{zoom}_{x}_{y}{scale}.png")
                if os.path.exists(tile_path):
                    row.append(Image.open(tile_path))
                else:
                    url = provider.build_url(x=x, y=y, z=zoom, scale_factor=scale, apikey=api_key)
                    response = requests.get(url)
                    if response.status_code == 200 and "image" in response.headers.get("Content-Type", ""):
                        tile_image = Image.open(BytesIO(response.content))
                        tile_image.save(tile_path)
                        row.append(tile_image)
                    else:
                        try:
                            row.append(Image.new(mode=tiles[0][0].mode, size=tiles[0][0].size))
                        except:
                            raise Exception(f"Failed to download map tile: {response.status_code} - {response.text}")
            tiles.append(row)

        return tiles, tile_bounds_margin

    def stitch_tiles(self, tiles, gpx_bounds: GPXBoundsBound, tile_bounds_margin: TileBounds, zoom: int):
        tile_width, tile_height = tiles[0][0].size
        map_width = tile_width * len(tiles[0])
        map_height = tile_height * len(tiles)
        map_image = Image.new("RGB", (map_width, map_height))

        for i, row in enumerate(tiles):
            for j, tile in enumerate(row):
                if tile:
                    map_image.paste(tile, (j * tile_width, i * tile_height))

        return map_image

    def add_map_image(self, fig, map_image, min_latlon_tiles, max_latlon_tiles):
        buffered = BytesIO()
        map_image.save(buffered, format="PNG")
        map_image_data_uri = "data:image/png;base64," + base64.b64encode(buffered.getvalue()).decode()

        fig.add_layout_image(
            dict(
                source=map_image_data_uri,
                xref="x",
                yref="y",
                x=min_latlon_tiles["lon"],
                y=max_latlon_tiles["lat"],
                sizex=np.abs(max_latlon_tiles["lon"] - min_latlon_tiles["lon"]),
                sizey=np.abs(max_latlon_tiles["lat"] - min_latlon_tiles["lat"]),
                sizing="contain",
                opacity=1,
                layer="below",
            ),
        )

    def add_gpx_track(self, fig, df):
        fig.add_trace(
            go.Scatter(
                x=df["longitude"],
                y=df["latitude"],
                mode="lines",
                line=dict(width=8, color=self.config["secondary_accent_color"]),
                name="GPX Track",
                xaxis="x",
                yaxis="y",
            ),
        )

    def draw_data_box(self, fig: go.Figure, title: str, data: str, pos: tuple[float, float]) -> None:
        fig.add_annotation(
            dict(
                text=title,
                xref="paper",
                yref="paper",
                x=pos[0],
                y=pos[1],
                showarrow=False,
                align="center",
                xanchor="center",
                yanchor="bottom",
                font=dict(
                    family="Brandon Grotesque",
                    size=42,
                    weight=400,
                    textcase="upper",
                    color=self.config["primary_text_color"],
                ),
            ),
        )

        fig.add_annotation(
            dict(
                text=data,
                xref="paper",
                yref="paper",
                x=pos[0],
                y=pos[1] + 0.035,
                showarrow=False,
                align="center",
                xanchor="center",
                yanchor="bottom",
                font=dict(
                    family="Brandon Grotesque",
                    size=58,
                    weight=600,
                    color=self.config["primary_accent_color"],
                ),
            ),
        )

    def add_data_boxes(self, fig: go.Figure, gpx_data: GpxData) -> None:
        self.draw_data_box(fig, "Distance", gpx_data["distance"], (0.65, 0.6))
        self.draw_data_box(fig, "Moving time", gpx_data["moving_time"], (0.85, 0.6))
        self.draw_data_box(fig, "Start time", gpx_data["start_time_local"], (0.65, 0.45))
        self.draw_data_box(fig, "End time", gpx_data["end_time_local"], (0.85, 0.45))
        self.draw_data_box(fig, "Average Speed", gpx_data["average_moving_speed"], (0.65, 0.30))
        self.draw_data_box(fig, "Average HR", gpx_data["average_heartrate"], (0.85, 0.30))
        self.draw_data_box(fig, "Elevation Gain", gpx_data["elevation_gain"], (0.65, 0.15))
        self.draw_data_box(fig, "Elevation Loss", gpx_data["elevation_loss"], (0.85, 0.15))

    def add_elevation_plot(self, fig: go.Figure, df: pd.DataFrame) -> tuple[list, list]:
        elevation_y_span = df["elevation"].max() - df["elevation"].min()
        elevation_y_range = [
            df["elevation"].min() - elevation_y_span * 0.175,
            df["elevation"].max() + elevation_y_span * 0.35,
        ]
        elevation_x_span = df["distance_from_start"].max() - df["distance_from_start"].min()
        elevation_x_range = [
            df["distance_from_start"].min() - elevation_x_span * 0.17,
            df["distance_from_start"].max() + elevation_x_span * 0.05,
        ]

        radius_x = (elevation_x_range[1] - elevation_x_range[0]) * 0.008
        radius_y = (elevation_y_range[1] - elevation_y_range[0]) * 0.024

        line_thickness = 2
        behind_white_box_xrange = [
            elevation_x_range[0] - elevation_x_span * line_thickness / 100 / 5,
            elevation_x_range[1] + elevation_x_span * line_thickness / 100 / 5,
        ]
        behind_white_box_yrange = [
            elevation_y_range[0] - elevation_y_span * line_thickness / 100,
            elevation_y_range[1] + elevation_y_span * line_thickness / 100,
        ]

        x_points = [
            behind_white_box_xrange[0] + radius_x,
            behind_white_box_xrange[0] + radius_x,
            behind_white_box_xrange[1] - radius_x,
            behind_white_box_xrange[1] - radius_x,
            behind_white_box_xrange[1] - radius_x / np.pi,
            behind_white_box_xrange[1],
            behind_white_box_xrange[1],
            behind_white_box_xrange[1],
            behind_white_box_xrange[1],
            behind_white_box_xrange[1] - radius_x / np.pi,
            behind_white_box_xrange[1] - radius_x,
            behind_white_box_xrange[1] - radius_x,
            behind_white_box_xrange[0] + radius_x,
            behind_white_box_xrange[0] + radius_x,
            behind_white_box_xrange[0] + radius_x / np.pi,
            behind_white_box_xrange[0],
            behind_white_box_xrange[0],
            behind_white_box_xrange[0],
            behind_white_box_xrange[0],
            behind_white_box_xrange[0] + radius_x / np.pi,
            behind_white_box_xrange[0] + radius_x,
        ]

        y_points = [
            behind_white_box_yrange[0],
            behind_white_box_yrange[0],
            behind_white_box_yrange[0],
            behind_white_box_yrange[0],
            behind_white_box_yrange[0] + radius_y / np.pi,
            behind_white_box_yrange[0] + radius_y,
            behind_white_box_yrange[0] + radius_y,
            behind_white_box_yrange[1] - radius_y,
            behind_white_box_yrange[1] - radius_y,
            behind_white_box_yrange[1] - radius_y / np.pi,
            behind_white_box_yrange[1],
            behind_white_box_yrange[1],
            behind_white_box_yrange[1],
            behind_white_box_yrange[1],
            behind_white_box_yrange[1] - radius_y / np.pi,
            behind_white_box_yrange[1] - radius_y,
            behind_white_box_yrange[1] - radius_y,
            behind_white_box_yrange[0] + radius_y,
            behind_white_box_yrange[0] + radius_y,
            behind_white_box_yrange[0] + radius_y / np.pi,
            behind_white_box_yrange[0],
        ]

        fig.add_trace(
            go.Scatter(
                x=x_points,
                y=y_points,
                line=dict(width=0, color=self.config["elevation_color_shadow"], shape="spline"),
                fill="toself",
                fillcolor=self.config["elevation_color_shadow"],
                mode="lines",
                showlegend=False,
                xaxis="x2",
                yaxis="y2",
            ),
        )

        x_points = [
            elevation_x_range[0] + radius_x,
            elevation_x_range[0] + radius_x,
            elevation_x_range[1] - radius_x,
            elevation_x_range[1] - radius_x,
            elevation_x_range[1] - radius_x / np.pi,
            elevation_x_range[1],
            elevation_x_range[1],
            elevation_x_range[1],
            elevation_x_range[1],
            elevation_x_range[1] - radius_x / np.pi,
            elevation_x_range[1] - radius_x,
            elevation_x_range[1] - radius_x,
            elevation_x_range[0] + radius_x,
            elevation_x_range[0] + radius_x,
            elevation_x_range[0] + radius_x / np.pi,
            elevation_x_range[0],
            elevation_x_range[0],
            elevation_x_range[0],
            elevation_x_range[0],
            elevation_x_range[0] + radius_x / np.pi,
            elevation_x_range[0] + radius_x,
        ]

        y_points = [
            elevation_y_range[0],
            elevation_y_range[0],
            elevation_y_range[0],
            elevation_y_range[0],
            elevation_y_range[0] + radius_y / np.pi,
            elevation_y_range[0] + radius_y,
            elevation_y_range[0] + radius_y,
            elevation_y_range[1] - radius_y,
            elevation_y_range[1] - radius_y,
            elevation_y_range[1] - radius_y / np.pi,
            elevation_y_range[1],
            elevation_y_range[1],
            elevation_y_range[1],
            elevation_y_range[1],
            elevation_y_range[1] - radius_y / np.pi,
            elevation_y_range[1] - radius_y,
            elevation_y_range[1] - radius_y,
            elevation_y_range[0] + radius_y,
            elevation_y_range[0] + radius_y,
            elevation_y_range[0] + radius_y / np.pi,
            elevation_y_range[0],
        ]

        fig.add_trace(
            go.Scatter(
                x=x_points,
                y=y_points,
                line=dict(width=0, color=self.config["elevation_color"]),
                fill="toself",
                fillcolor="rgba(255,255,255,1)",
                mode="lines",
                showlegend=False,
                xaxis="x2",
                yaxis="y2",
            ),
        )

        fig.add_trace(
            go.Scatter(
                x=[df["distance_from_start"].iat[0]]
                + [df["distance_from_start"].iat[0]]
                + df["distance_from_start"].tolist()
                + [df["distance_from_start"].iat[-1]]
                + [df["distance_from_start"].iat[-1]]
                + [df["distance_from_start"].iat[0]],
                y=[df["elevation"].min()]
                + [df["elevation"].min()]
                + df["elevation"].tolist()
                + [df["elevation"].min()]
                + [df["elevation"].min()]
                + [df["elevation"].min()],
                mode="lines",
                line=dict(width=2, color=self.config["elevation_color"], shape="spline"),
                fill="toself",
                showlegend=False,
                xaxis="x2",
                yaxis="y2",
                name="Elevation",
            ),
        )

        return (behind_white_box_xrange, behind_white_box_yrange)

    def add_title_annotations(self, fig: go.Figure, gpx_data: GpxData) -> None:

        # Add type of track icon
        if self.annotations["type"] == "hike":
            icon = Image.open("icons/icons8-trekking-100.png")
        elif self.annotations["type"] == "bike":
            icon = Image.open("icons/icons8-cycling-100.png")
        else:
            icon = Image.new("RGB", (100, 100), color=(255, 255, 255))  # type: ignore

        fig.add_layout_image(
            dict(
                source=icon,
                xref="paper",
                yref="paper",
                x=0.75,
                y=self.config["titles_y_pos"] - 0.095,
                sizex=0.05,
                sizey=0.05,
                xanchor="center",
                yanchor="top",
            )
        )

        fig.add_annotation(
            dict(
                text=self.annotations["titles"]["title"],
                xref="paper",
                yref="paper",
                x=0.75,
                y=self.config["titles_y_pos"],
                showarrow=False,
                xanchor="center",
                font=dict(
                    family="Brandon Grotesque",
                    size=80,
                    weight=600,
                    textcase="upper",
                    color=self.config["primary_text_color"],
                ),
            ),
        )

        fig.add_annotation(
            dict(
                text=(
                    gpx_data["date"][0]
                    if gpx_data["date"][0] == gpx_data["date"][1]
                    else gpx_data["date"][0] + " - " + gpx_data["date"][1]
                ),
                xref="paper",
                yref="paper",
                x=0.75,
                y=self.config["titles_y_pos"] - 0.05,
                showarrow=False,
                xanchor="center",
                font=dict(
                    family="Brandon Grotesque",
                    size=55,
                    weight=400,
                    # textcase="upper",
                    color=self.config["primary_accent_color"],
                ),
            ),
        )

        fig.add_annotation(
            dict(
                text=self.annotations["titles"]["map_title"],
                xref="paper",
                yref="paper",
                x=0.25,
                y=self.config["titles_y_pos"],
                showarrow=False,
                xanchor="center",
                font=dict(
                    family="Brandon Grotesque",
                    size=80,
                    weight=600,
                    shadow="auto",
                    textcase="upper",
                    color=self.config["primary_text_color"],
                ),
            ),
        )

    def add_gpx_annotations(self, fig: go.Figure, df: pd.DataFrame) -> None:
        for annotation in self.annotations["gpx"]:
            idx = int(len(df) * annotation["track_pos"])
            # Add a dot at the annotation position
            fig.add_trace(
                go.Scatter(
                    x=[df.iloc[idx]["longitude"]],
                    y=[df.iloc[idx]["latitude"]],
                    mode="markers",
                    marker=dict(
                        size=18,
                        color=self.config["primary_text_color"],
                        symbol="circle",
                    ),
                    showlegend=False,
                )
            )

            # Add the text annotation without an arrow
            if "text_align" in annotation:
                xanchor = annotation["text_align"]
            else:
                xanchor = "left"
            if "text_valign" in annotation:
                yanchor = annotation["text_valign"]
            else:
                yanchor = "bottom"

            fig.add_annotation(
                dict(
                    text=annotation["text"],
                    xref="x",
                    yref="y",
                    x=df.iloc[idx]["longitude"],
                    y=df.iloc[idx]["latitude"],
                    showarrow=False,
                    font=dict(
                        family="Brandon Grotesque",
                        size=32,
                        weight=500,
                        color=self.config["primary_text_color"],
                        shadow="auto",
                    ),
                    yanchor=yanchor,
                    xanchor=xanchor,
                    yshift=10,  # Adjust this value to position the text above the dot
                ),
            )

    def plot_gpx_data(
        self,
        df,
        gpx_data: GpxData,
        map_image: Image.Image,
        tile_bounds_margin: TileBounds,
        gpx_bounds: GPXBoundsBound,
        zoom: int,
    ):
        min_latlon_tiles = self.tile_to_lat_lon(
            tile_id={"xtile": tile_bounds_margin["left_xtile"], "ytile": tile_bounds_margin["bot_ytile"] + 1},
            zoom=zoom,
        )
        max_latlon_tiles = self.tile_to_lat_lon(
            tile_id={"xtile": tile_bounds_margin["right_xtile"] + 1, "ytile": tile_bounds_margin["top_ytile"]},
            zoom=zoom,
        )

        fig = go.Figure()

        self.add_map_image(fig, map_image, min_latlon_tiles, max_latlon_tiles)
        self.add_gpx_track(fig, df)

        behind_white_box_xrange, behind_white_box_yrange = self.add_elevation_plot(fig, df)

        self.add_data_boxes(fig, gpx_data)

        self.add_title_annotations(fig, gpx_data)
        self.add_gpx_annotations(fig, df)

        upper_margin, lower_margin = self.calculate_map_margins()
        gpx_lon_margin = np.abs(gpx_bounds.max_longitude - gpx_bounds.min_longitude) * self.config["gpx_margin"]
        gpx_lat_margin = np.abs(gpx_bounds.max_latitude - gpx_bounds.min_latitude) * self.config[
            "gpx_margin"
        ] + np.abs(gpx_bounds.max_latitude - gpx_bounds.min_latitude) / (
            1 - upper_margin - lower_margin
        )  # FIXME instead of dividing by 2 this should be an upper and lower margin

        aspect_ratio = (
            np.abs(max_latlon_tiles["lon"] - min_latlon_tiles["lon"])
            / (np.abs(tile_bounds_margin["left_xtile"] - tile_bounds_margin["right_xtile"]) + 1)
        ) / (
            np.abs(max_latlon_tiles["lat"] - min_latlon_tiles["lat"])
            / (np.abs(tile_bounds_margin["top_ytile"] - tile_bounds_margin["bot_ytile"]) + 1)
        )

        a4_width_in_cm = 25
        a4_height_in_cm = a4_width_in_cm * self.config["aspect_ratio"]
        dpi = 300
        fig_width = int(a4_width_in_cm * dpi / 2.54)
        fig_height = int(a4_height_in_cm * dpi / 2.54)

        fig.update_layout(
            margin=dict(l=0, r=0, t=0, b=0),
            xaxis=dict(
                range=[gpx_bounds.min_longitude - gpx_lon_margin / 2, gpx_bounds.max_longitude + gpx_lon_margin / 2],
                constrain="range",
                visible=False,
                domain=[0, 0.5],
            ),
            yaxis=dict(
                range=[gpx_bounds.min_latitude - gpx_lat_margin / 2, gpx_bounds.max_latitude + gpx_lat_margin / 2],
                scaleanchor="x",
                scaleratio=aspect_ratio,
                visible=False,
                domain=[0, 1],
            ),
            xaxis2=dict(
                domain=self.config["elevation_x_domain"],
                anchor="y2",
                range=behind_white_box_xrange,
                matches=None,
                showgrid=False,
                zeroline=False,
                visible=False,
                title="distance_from_start",
            ),
            yaxis2=dict(
                domain=self.config["elevation_y_domain"],
                anchor="x2",
                matches=None,
                showgrid=False,
                zeroline=False,
                visible=True,
                showticklabels=True,
                tickfont=dict(
                    family="Brandon Grotesque",
                    color=self.config["elevation_color"],
                    weight=400,
                    size=36,
                    shadow="1px 1px 2px rgba(5, 70, 14, 0.25)",
                ),
                ticklabelstandoff=28,
                ticklabelshift=-15,
                ticklabeloverflow="allow",
                ticklabelposition="inside",
                tickvals=[int(df["elevation"].min()), int(df["elevation"].max())],
                ticktext=[
                    f"{int(df['elevation'].min())}m",
                    f"{int(df['elevation'].max())}m",
                ],
            ),
            width=fig_width,
            height=fig_height,
            showlegend=False,
            paper_bgcolor="rgba(255,255,255,255)",
            plot_bgcolor="rgba(0,0,0,0)",
        )

        fig.write_image(
            f"output/{self.annotations['titles']['map_title']} - {self.annotations['titles']['title']}.png",
            width=fig_width,
            height=fig_height,
            scale=1,
            engine="kaleido",
        )

        pio.show(fig, renderer="browser")

    def calculate_zoom_level(self, gpx_bounds: GPXBoundsBound) -> int:
        lat_span = gpx_bounds.max_latitude - gpx_bounds.min_latitude
        lon_span = gpx_bounds.max_longitude - gpx_bounds.min_longitude

        max_span = max(lat_span, lon_span)

        if max_span > 1.0:
            return 9
        elif max_span > 0.5:
            return 10
        elif max_span > 0.25:
            return 11
        elif max_span > 0.3:
            return 12
        elif max_span > 0.1:
            return 13
        elif max_span > 0.05:
            return 14
        else:
            return 15

    def plot(self):
        with open(self.gpx_file, "r") as gpx_file:
            gpx = gpxpy.parse(gpx_file)

        df, gpx_data = self.extract_gpx_data(gpx)

        gpx_bounds = gpx.get_bounds()
        if (
            gpx_bounds is None
            or gpx_bounds.max_latitude is None
            or gpx_bounds.max_longitude is None
            or gpx_bounds.min_latitude is None
            or gpx_bounds.min_longitude is None
        ):
            logging.error("GPX file does not contain valid bounds.")
            return

        gpx_bounds_bound = GPXBoundsBound(
            gpx_bounds.min_latitude,
            gpx_bounds.max_latitude,
            gpx_bounds.min_longitude,
            gpx_bounds.max_longitude,
        )

        zoom = self.calculate_zoom_level(gpx_bounds_bound)
        if "zoom" in self.annotations:
            zoom = self.annotations["zoom"]

        tiles, tile_bounds_margin = self.download_map_tiles(gpx_bounds=gpx_bounds_bound, zoom=zoom)

        map_image = self.stitch_tiles(tiles, gpx_bounds_bound, tile_bounds_margin, zoom)

        self.plot_gpx_data(df, gpx_data, map_image, tile_bounds_margin, gpx_bounds_bound, zoom)


if __name__ == "__main__":
    # files = [file for file in os.listdir("gpx_tracks") if file.endswith(".gpx")]
    # for file in files:
    #     plotter = GPXPlotter(
    #         gpx_file="gpx_tracks/" + file,
    #         annotations_file_path="gpx_tracks/" + file.replace(".gpx", "_annotations.json"),
    #     )
    #     print(f"Plotting {file}")
    # plotter.plot()
    plotter = GPXPlotter(
        gpx_file="gpx_tracks/rogueriver.gpx",
        annotations_file_path="gpx_tracks/rogueriver_annotations.json",
    )
    plotter.plot()
